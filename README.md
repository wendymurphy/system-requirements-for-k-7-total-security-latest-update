# System Requirements for K-7 Total Security #

K-7 Total Security is the best tool to prevent your PC from malicious and unwanted threats and keep it secure from every kind of spam, virus or bugs. You can download it from the official website or purchase its offline copy without any trouble, as it is available for everyone.

In this article, we are going to discuss the K7 system requirements for the Enterprise Security Administrator Console.

## Requirements: ##

### OS: ###

* Windows Server  - Any Version - 2003 (SP1 or higher)
* Windows Server – Any Version - 2008 
* Windows Server 2011 – Any Version (Small Business)
* Windows Vista / Windows 7/ XP Pro (SP2 or later)
* Windows Server 2016
* Windows Server 2012

### Database ###

* Business Version of K7  includes Express Version of MS-SQL Server 2005 ( inbuilt database)
* SQL Server 2008 of Microsoft or Server 2005 (Service Pack 2), can be used.

### Other Programs or browsers: ###

* IE 8.
* Google Chrome, Firefox, etc. can also be used. 

### Hardware ###

* Hard drive space: 700 MB
* RAM: 512 MB

## For Endpoint Protection: ##

### OS: ###

* Windows Server (SP1 or later) - Any Version - 2003 
* Windows Server – Any Version - 2008 
* Windows Server 2011 – Any Version (Small Business)
* Windows Vista / Windows 7/Windows 8/ XP Professional (SP2 or higher) 32 bit only 
* Windows Server 2016
* Windows Server 2012

### Hardware ###

* RAM(minimum) : 512 MB
* Space on Hard drive:  300 MB

### Web Filtering ###

Use these browsers for web filtering

* Chrome
* IE 8 or higher
* Firefox

## For Endpoint Protection: ##

### Installation- ###

First, download the setup file and then open the downloaded file to start the installation.

### Activation (Online)- ###

1. Click on the 'Next' button present on the screen of the product activation.
2. If your product copy is licensed and purchased, then click on the option that you have purchased the copy and have the valid serial number to install the product. 
3. Provide the details for activation like email address and name.
4. Type your product serial number and then click 'Next'
5. You can go back in case you wish to change the details you have entered.
6. Ensure that your system is connected to a network with Internet connection. Click on 'Next' to move forward.
7. You'll receive the validity details of license and account information.

### Activation (Offline)- ###

1. Click on 'Next' option on the screen of product activation.
2. If your product copy is licensed and purchased, then click on the option that you have purchased the copy and have the valid serial number to install the product. 
3. Provide the details for activation like email address and name.
4. Type your product serial number and then click 'Next'.
5. You can go back in case you wish to change the details you have entered.
6. Move forward, until you get the 'Activation Failed' message- Internet connection is not established.
7. Click on the “Offline activation' option, a system ID will be generated.
8. Call on 18004190077 once you receive the system ID.
9. Listen for the activation code and enter it in the offline activation screen.

### Update- ###

1. Open K7 total security and click on 'Update' option to update your product copy.

If you still have any issues with K7 antivirus then you can contact K7 Antivirus support tollfree number **([icustomerservice.net/k7-antivirus-support](https://icustomerservice.net/k7-antivirus-support/))**, we are 24 * 7 available to help you. Connect with our experts and get solutions to all your worries instantly.